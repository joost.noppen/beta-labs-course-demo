import unittest
from helloworld import helloWorld

class TestUnit(unittest.TestCase):
    def test_helloWorld(self):
        self.assertEqual(helloWorld(), "Hello World")

if __name__ == '__main__':
    unittest.main()
